﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Randomforce : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    void Start()
    {
         
 GetComponent<Rigidbody>().velocity = Random.onUnitSphere * speed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
