﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEnem : MonoBehaviour
{
    public Rigidbody rb;
    public Animator anim;
    public Vector3 lastPosition;
    public Vector3 velocity;

    // Start is called before the first frame update
    void Start()
    {
        lastPosition = transform.position;
        rb.GetComponent<Rigidbody>();
        anim.GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        velocity = (transform.position - lastPosition) / Time.deltaTime;


        
    }
     void LateUpdate() {
         lastPosition = transform.position; 
                 if(velocity.magnitude<=0.1f){
	anim.SetBool("isRunning",false);
}
else{
	anim.SetBool("isRunning",true);

}
        
    }
}
