﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemigoModelo : MonoBehaviour
{
    public EnemyBase enem;
    public GameObject bullet;
    public float bulletspeed = 10;
    public float timer;
    public float fireRate;
    public Transform firepoint;
    public Vector3 _dir;
    public float turnSpeed = 210;
    public int life;
    public int damage;
    public Rigidbody rb;
    public int maxlife;
    public GameObject target;
    public Transform navtarget;
    public float speed = 2f;
    public float minDistance = 1f;
    public float range;
    public NavMeshAgent navAgent;
    public bool isChasing;
    public float seekRange;
    public Transform player;
    public Transform patrolPoint;
    public int knockbackspeed;
    public GameObject gib;
  

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
}

