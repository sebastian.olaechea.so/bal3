﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBase : MonoBehaviour
{
    public int life;
    public int damage;
    private Rigidbody rb;
    public int maxlife;
    public GameObject target;
    public Transform navtarget;
    public float speed = 2f;
    private float minDistance = 1f;
    public float range;
    private NavMeshAgent navAgent;
    public bool CanChase;
    public float seekRange;
    public Transform player;
    // Use this for initialization

    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        player=GameObject.FindGameObjectWithTag("Player").transform;
        navAgent.updatePosition = false;
        navAgent.updateRotation = true;
        navAgent.stoppingDistance = range;
        rb = GetComponent<Rigidbody>();
        life = maxlife;
       
        
    }

    // Update is called once per frame
    void Update()
    {
        MoveToTarget();
        if (life <= 0)
        {
            Destroy(gameObject);
        }
         float dist = Vector3.Distance(transform.position, player.transform.position);
     if (dist < seekRange){
         InvokeRepeating("SeekTarget",0.1f,0.1f); 
     }
     if(dist>seekRange){
         CancelInvoke("SeekTarget");
     
     }

    }
    private void FixedUpdate()
    {
        rb.position = (transform.position + navAgent.velocity * Time.fixedDeltaTime);
        navAgent.nextPosition = rb.position; 
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag=="BulletPlayer")
        {
            life -= other.gameObject.GetComponent<Bullet>().damage;
        }
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().life -= damage;
        }
    }
    void MoveToTarget()
    {
    
      if(navtarget!=null)
        navAgent.SetDestination(navtarget.position);
    }
    void SeekTarget(){
         navtarget = GameObject.FindGameObjectWithTag("Player").transform;
    }
}



	

