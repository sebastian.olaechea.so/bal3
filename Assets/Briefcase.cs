﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Briefcase : MonoBehaviour
{
    public Vector3 RotateAmount;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameMaster.ObjTaken)
        transform.Rotate(RotateAmount * Time.deltaTime);

    }

 private void OnTriggerEnter(Collider other)
{
            if(other.gameObject.tag=="Player"){
            Destroy(GetComponent<Collider>());
           transform.position= GameObject.Find("BriefSpot").transform.position;
           transform.parent=GameObject.Find("BriefSpot").transform;
           transform.rotation= GameObject.Find("BriefSpot").transform.rotation;
        
           
           GameMaster.ObjTaken=true;
}
}}

