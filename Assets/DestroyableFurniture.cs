﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableFurniture : MonoBehaviour
{
    public int life;
    public GameObject gib;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(life<=0){
            Instantiate(gib,transform.position,transform.rotation);
            Destroy(gameObject);
        }
    }
     private void OnCollisionEnter(Collision other) {
        if(other.gameObject.tag=="BulletPlayer"||other.gameObject.tag=="Bullet"){
            life--;
        }
    }
}
