﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float MoveSpeed;
	public GameObject projectile;
	public Transform shootPoint;
	public float shootspeed;
	public float timer=0.5f;
	public float fireRate;
	public float maxlife;
	public float life;
	public float _horizontal;
	public  float _vertical;
	public Animator anim;
	public float knockbackTime;
	public float knockbackCounter;
	public LayerMask hitmask;
	
	public Rigidbody rb;
	// Use this for initialization
	void Start () {
		
		rb=GetComponent<Rigidbody>();
		life=maxlife;
		
	}
	
	// Update is called once per frame
	void Update () {
        //MoveInput();
        if (knockbackCounter <= 0)
            MoveInput();
        //MouseInput();
        else
        {
            knockbackCounter -= Time.deltaTime;
        }
        MouseInput();
		Shoot();
		Timer();
		if(life<=0)
		Die();
if(rb.velocity==Vector3.zero){
	anim.SetBool("isRunning",false);
}
else{
	anim.SetBool("isRunning",true);

}
		
		
	}

	void MoveInput(){
		 _horizontal = Input.GetAxis("Horizontal");
		 _vertical = Input.GetAxis("Vertical");
		Vector3 _movement = new Vector3(_horizontal,rb.velocity.y, _vertical);

		///transform.Translate(_movement*MoveSpeed*Time.deltaTime, Space.World);
		 rb.velocity=(_movement * MoveSpeed);
	}
	void MouseInput(){
		Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit _hit;
		if(Physics.Raycast(_ray, out _hit,1000,hitmask)){
			transform.LookAt(new Vector3(_hit.point.x,transform.position.y,_hit.point.z));
		}
	}
	void Timer(){
		timer-=Time.deltaTime;


	}
	void Shoot(){
		if(Input.GetKey(KeyCode.Mouse0)&&timer<=0){
		GameObject proj=Instantiate(projectile,shootPoint.position,transform.rotation);
		proj.GetComponent<Rigidbody>().AddForce(shootPoint.forward*shootspeed,ForceMode.Impulse);
		timer=fireRate;
		}
		

	}
	void Die(){
		Debug.Log("Morido");
        GetComponent<PlayerController>().enabled=false;
		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
	}


}
