﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    private EnemigoModelo enem;
    public float startWaitTime;
    public Transform moveSpot;
    public   float waitTime;
    public float minX;
    public float maxX;
    public float minZ;
    public float maxZ;
    public bool arrived;
    
    // Start is called before the first frame update
    void Start()

    {
        enem=GetComponent<EnemigoModelo>();
        waitTime=startWaitTime;
        if(enem.navtarget!=null)
        enem.navtarget.transform.position= new Vector3(Random.Range(minX,maxX),0,Random.Range(minZ,maxZ));
       
    }

    // Update is called once per frame
    void Update()
    
    {
        if(enem.isChasing){
            return;
        }
        timer();

        if (waitTime>=0)
        {
            return;
        }
        if (waitTime <= 0)
        {
            waitTime=startWaitTime;
        }
        if (enem.navtarget != null)
        {
            return;
        }
            if (enem.navtarget.gameObject.tag == "Player")
                return;
        
        if (Vector3.Distance(transform.position, enem.navtarget.transform.position) <4f && !enem.isChasing){
            arrived = true;
            enem.navtarget.transform.position = new Vector3(Random.Range(minX, maxX), 0, Random.Range(minZ, maxZ));
            waitTime =startWaitTime;
        }
        if (Vector3.Distance(transform.position, enem.navtarget.transform.position) > 4f)
        {
            

        }

    }
    public void timer()
    {
        waitTime -= Time.deltaTime;
    }
}
