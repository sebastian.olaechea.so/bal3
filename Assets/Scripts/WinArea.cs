﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinArea : MonoBehaviour
{
    public GameObject winText;
    private PauseMenu pause;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="Player"){
            if(GameMaster.ObjTaken==true){
                winText.SetActive(true);
                Destroy(GameObject.Find("ResumeButton"));
                pause.pauseMenuUI.SetActive(true);
                Time.timeScale = 0f;
                PauseMenu.GameIsPaused = true;
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().enabled=false;
            }
        }
    }
}
