﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float timer;
    public float Spawntime;
    public bool canSpawn;
    public bool hasBeenSeen;
    public GameObject enemyToSpawn;
    public int maximum = 3;
    public Transform PatrolPoint;
    public GameMaster gm;
    public float TimerModifier;


    public float spotTime=30;
    public List<GameObject> m_list = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        canSpawn=true;
        gm=GameObject.Find("GameManager").GetComponent<GameMaster>();
    }

    // Update is called once per frame
    void Update()
    {
        Timer();
        Spawn();
        if(m_list.Count==maximum){
        canSpawn=false;
        }
        if(m_list.Count<maximum){
        canSpawn=true;
    }
    }
    void Timer() {
        timer = (timer - (TimerModifier*gm.timer/60))-Time.deltaTime;
	}
    void Spawn(){
        if(timer<=0 && canSpawn && !hasBeenSeen){
       GameObject obj= Instantiate(enemyToSpawn,transform.position,transform.rotation);
       obj.GetComponent<EnemigoModelo>().navtarget=PatrolPoint;
       m_list.Add(obj);

        timer=Spawntime;
        }
    }
     void LateUpdate()
     {
          //remove all destroyed objects
          m_list.RemoveAll(o => (o == null || o.Equals(null)) );
     }
    void OnBecameVisible()
    {
        timer=spotTime;
        hasBeenSeen=true;
        Debug.Log("You see me");
    }

    void OnBecameInvisible()
    {
        hasBeenSeen=false;
        Debug.Log("You no see me");
        timer=spotTime;
    }
}
