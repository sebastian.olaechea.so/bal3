﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody rb;
    public int damage;
    // Start is called before the first frame update
    void Start()

    {
        rb = GetComponent<Rigidbody>();

        Destroy(gameObject,1);
    }

    // Update is called once per frame
    void Update()
    {
      


    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player"){
            collision.gameObject.GetComponent<PlayerModelo>().life -= damage;
    }
        else
        Destroy(gameObject);
    }
}
