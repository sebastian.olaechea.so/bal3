﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class alarmLight : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3 RotateAmount;
    public Component[] lights;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameMaster.spotted){
      lights = gameObject.GetComponentsInChildren<Light>();
      foreach(Light lig in lights){
          lig.enabled=true;
      }
     
        transform.Rotate(RotateAmount * Time.deltaTime);
        
        }
    }
    
}
