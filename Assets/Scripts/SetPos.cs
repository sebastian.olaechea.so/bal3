﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPos : MonoBehaviour
{
    private Transform trens;
    // Start is called before the first frame update
    void Start()
    {
        trens = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, -0.2f, transform.position.z);
        
    }
}
