﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gibs : MonoBehaviour
{
    private Rigidbody rb;
    public int Gib_time;
    private bool deleted;
    public float timer;
    private Collider csd;
    // Start is called before the first frame update
    void Start()
    {
        rb=GetComponent<Rigidbody>();
        csd=GetComponent<MeshCollider>();
        Destroy(gameObject,Gib_time);
        
        
    }

    // Update is called once per frame
    void Update()
    {
 
    }
    void OnBeconeInvisible(){
        Destroy(gameObject);
    }
    void gibDelete(){
        
        Destroy(GetComponent<Rigidbody>());
        Destroy(csd);
        transform.Translate((Vector3.down*0.01f)*Time.deltaTime,Space.World);
        Destroy(gameObject,Gib_time);
        
        
       
    }
        public void Timer()
    {
        timer += Time.deltaTime;
    }
}
