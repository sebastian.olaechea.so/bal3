﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LifeDisplay : MonoBehaviour
{
    public Text text;
    private PlayerModelo pla;
    
    // Start is called before the first frame update
    void Start()
    {
         text= GetComponent<Text>();
         pla=GameObject.Find("Player").GetComponent<PlayerModelo>();
    }

    // Update is called once per frame
    void Update()
    {
        text.text=("Life: "+pla.life.ToString());
    }
}
