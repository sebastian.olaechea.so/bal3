﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour {
	public Transform target;
	public Vector3 Offset;
	public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		MoveCamera();
		
	}
	void MoveCamera(){
		transform.position= Vector3.Lerp(transform.position,target.position+Offset,speed* Time.deltaTime);
	}
}
