﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRanged : MonoBehaviour
{
    private EnemyBase enem;
    public GameObject bullet;
    public float bulletspeed=10;
    public float timer;
    public float fireRate;
    public Transform firepoint;
    private Vector3 _dir;
    public float turnSpeed=210;
    // Start is called before the first frame update
    void Start()
    {
        enem=GetComponent<EnemyBase>();
        
    }

    // Update is called once per frame
    void Update()
    {
        Timer();
        float dist1 = Vector3.Distance(transform.position, enem.player.transform.position);
     if (dist1 < enem.seekRange && timer<=0){

        
        StartCoroutine(ShootAtPlayer());
     }
       if (dist1 < enem.seekRange){
           LookAtPlayer();

       }
    
    }
    
    
    private IEnumerator ShootAtPlayer(){



       yield return new WaitForSeconds(0.3f);
        if (timer<= 0){
            GameObject bul = Instantiate(bullet, firepoint.position, transform.rotation);
            bul.GetComponent<Rigidbody>().AddForce(firepoint.right * bulletspeed, ForceMode.Impulse);
            timer = fireRate;
        }
      
        

    }
    void Timer(){
		timer-=Time.deltaTime;
	}
    void LookAtPlayer(){
        Vector3 targetDir = enem.player.position - transform.position;

        // The step size is equal to speed times frame time.
        float step = turnSpeed * Time.deltaTime;

        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
        Debug.DrawRay(transform.position, newDir, Color.red);

        // Move our position a step closer to the target.
        transform.rotation = Quaternion.LookRotation(newDir);
        
       
    }
}




