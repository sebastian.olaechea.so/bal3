﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disable : MonoBehaviour
{
    public CamaraController cam;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(disable());  
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator disable()
    {
        cam.enabled = false;
        GameObject.Find("Player").GetComponent<PlayerController>().enabled = false;
        yield return new WaitForSeconds(3);
        cam.enabled = true;
        yield return new WaitForSeconds(5);
        GameObject.Find("Player").GetComponent<PlayerController>().enabled = true   ;
        cam.speed = 6;
        Destroy(GetComponent<Disable>());
    }
}
