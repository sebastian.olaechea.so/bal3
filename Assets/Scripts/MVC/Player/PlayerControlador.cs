﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControlador : MonoBehaviour
{
    private PlayerModelo pm;
    private void Start()
    {
        pm = GetComponent<PlayerModelo>();
        pm.rb = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    public void MoveInput()
    {
        pm._horizontal = Input.GetAxis("Horizontal");
        pm._vertical = Input.GetAxis("Vertical");
        Vector3 _movement = new Vector3(pm._horizontal, pm.rb.velocity.y, pm._vertical);

        ///transform.Translate(_movement*MoveSpeed*Time.deltaTime, Space.World);
        pm.rb.velocity = (_movement * pm.MoveSpeed);
    }
    public void MouseInput()
    {
        Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit _hit;
        if (Physics.Raycast(_ray, out _hit, 1000, pm.hitmask))
        {
            transform.LookAt(new Vector3(_hit.point.x, transform.position.y, _hit.point.z));
        }
    }
    public void Timer()
    {
        pm.timer -= Time.deltaTime;


    }
    public void Shoot()
    {
        if (Input.GetKey(KeyCode.Mouse0) && pm.timer <= 0)
        {
            GameObject proj = Instantiate(pm.projectile, pm.shootPoint.position, transform.rotation);
            proj.GetComponent<Rigidbody>().AddForce(pm.shootPoint.forward * pm.shootspeed, ForceMode.Impulse);
            pm.timer = pm.fireRate;
        }


    }
    public void LoadScene()
    {
        SceneManager.LoadScene("Lose");
    }
    public void Die()
    {
        Debug.Log("Morido");
        GetComponent<PlayerVista>().enabled = false;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        pm.life = 0;
        Invoke("LoadScene",1);
    }

}
